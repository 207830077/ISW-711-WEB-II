const express = require('express');
const bodyparser = require('body-parser');
const cors = require("cors");
const fetch = require('node-fetch');
const dotenv = require('dotenv');
const connectDB = require('./database/connection');
const Session = require("./model/sessionModel");

const app = express();

const {
    saveSession,
    destroySession
  } = require("./controller/sessionController.js");

dotenv.config({path : 'config.env'});
const PORT = process.env.PORT || 8080;

connectDB();

//parsea el body
app.use(bodyparser.urlencoded({extended : true}));

app.use(cors({
  domains: '*',
  methods: "*"
}));

/**
 * Login for a token based authentication
 */
 app.post("/api/session", function (req, res) {
    if (req.body.username && req.body.password &&
      req.body.username === 'admin' && req.body.password === '123') {
      // insert token to the session table
      const token = saveSession(req.body.username);
      if(!token){
          res.status(422);
          res.json('Error');
      }
      res.status(201).send({
        token
      });
    } else {
      res.status(422);
      res.send({
        error: "Invalid username or password"
      });
    }
  });

  /**
 * Logout session
 */
app.delete("/api/session", function (req, res) {
    if (req.headers["authorization"]) {
      const token = req.headers['authorization'].split(' ')[1];
      // insert token to the session table
      destroySession(token);
      res.status(204).send({});
    } else {
      res.status(401);
      res.send({
        error: "Unauthorized "
      });
    }
  });

// Token based Auth Middleware
app.use(function (req, res, next) {
    if (req.headers["authorization"]) {
      const token = req.headers['authorization'].split(' ')[1];
      try {
        //validate if token exists in the database
        Session.findOne({ token }, function (error, session) {
          if (error) {
            console.log('error', error);
            res.status(401);
            res.send({
              error: "Unauthorized "
            });
          }
          if (session) {
            next();
            return;
          } else {
            res.status(401);
            res.send({
              error: "Unauthorized "
            });
          }
        });
      } catch (e) {
        res.status(422);
        res.send({
          error: "There was an error: " + e.message
        });
      }
    } else {
      res.status(401);
      res.send({
        error: "Unauthorized "
      });
    }
  });

app.get('/brewe', async (req, res) => {
  try {

    const response = await fetch('https://api.openbrewerydb.org/breweries?per_page=10');
    const json = await response.json();
    
    res.status(200);
    res.json(json);

  } catch (error) {
    console.log(error.response.body);
  }
});

app.listen(PORT, ()=>{
    console.log(`Servidor corriendo en localhost:${PORT}`);
});