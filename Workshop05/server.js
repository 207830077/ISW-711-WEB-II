const {graphqlExpress, graphiqlExpress} =  require('graphql-server-express');
const {makeExecutableSchema} = require('graphql-tools');
const {MongoClient, ObjectId} = require('mongodb');

const express = require('express');
const morgan = require('morgan');
const bodyparser = require('body-parser'); 

const dotenv = require('dotenv');
//const MONGO_URL = process.env.URL;
dotenv.config({path : 'config.env'});
const PORT = process.env.PORT || 8080;

const app = express();
app.use(morgan('tiny'));
app.use(bodyparser.urlencoded({extended : true}));

const db =  MongoClient.connect('mongodb://localhost:27017/blog')
const Posts = db.collection('posts')
const Comments = db.collection('comments')

const typeDefs = [`
type Query {
  post(_id: String): Post
  posts: [Post]
  comment(_id: String): Comment
}      

type Post {
  _id: String
  title: String
  content: String
  comments: [Comment]
}

type Comment {
  _id: String
  postId: String
  content: String
  post: Post
}

type Mutation {
  createPost(title: String, content: String): Post
  createComment(postId: String, content: String): Comment
}

schema {
  query: Query
  mutation: Mutation
}
`];

const resolvers = {
    Query: {
      post: async (root, {_id}) => {
        return prepare(await Posts.findOne(ObjectId(_id)))
      },
      posts: async () => {
        return (await Posts.find({}).toArray()).map(prepare)
      },
      comment: async (root, {_id}) => {
        return prepare(await Comments.findOne(ObjectId(_id)))
      },
    },
    Post: {
      comments: async ({_id}) => {
        return (await Comments.find({postId: _id}).toArray()).map(prepare)
      }
    },
    Comment: {
      post: async ({postId}) => {
        return prepare(await Posts.findOne(ObjectId(postId)))
      }
    },
    Mutation: {
      createPost: async (root, args, context, info) => {
        const res = await Posts.insertOne(args)
        return prepare(res.ops[0])
      },
      createComment: async (root, args) => {
        const res = await Comments.insert(args)
        return prepare(await Comments.findOne({_id: res.insertedIds[1]}))
      },
    },
  }

const schema = makeExecutableSchema({
typeDefs,
resolvers
})

const prepare = (o) => {
    o._id = o._id.toString()
    return o
}

const cors = require("cors");
app.use(cors({
domains: '*',
methods: "*"
}));

app.use('/graphql', bodyParser.json(), graphqlExpress({schema}))


app.use(homePath, graphiqlExpress({
  endpointURL: '/graphql'
}))

app.listen(PORT, () => {
  console.log(`Visit ${URL}:${PORT}${homePath}`)
})
