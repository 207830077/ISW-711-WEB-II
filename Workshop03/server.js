const express = require('express');
const morgan = require('morgan');
const bodyparser = require('body-parser'); 
const dotenv = require('dotenv');
const path = require('path'); 
const methodOverride = require('method-override'); 
const connectDB = require('./server/database/connections');

const app = express();

dotenv.config({path : 'config.env'});
const PORT = process.env.PORT || 8080;

app.use(morgan('tiny'));

connectDB(); 

app.use(bodyparser.urlencoded({extended : true}));
//app.use(methodOverride('_method'));

app.set('view engine', 'ejs');

app.use('/css', express.static(path.resolve(__dirname, 'assets/css')));
app.use('/img', express.static(path.resolve(__dirname, 'assets/img')));
app.use('/js', express.static(path.resolve(__dirname, 'assets/js')));

app.use('/', require('./server/routes/router'));

app.listen(PORT, ()=>{
    console.log(`Servidor corriendo en localhost:${PORT}`);
});