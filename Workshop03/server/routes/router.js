const express = require('express');
const route = express.Router();

const services = require('../services/render');
const controller = require('../controller/courseController'); 

route.get('/', services.homeRoutes);
route.get('/add-course', services.add_course);
route.get('/update-course', services.update_course);

//API
route.post('/api/courses', controller.create);
route.get('/api/courses', controller.find);
route.put('/api/courses/:id', controller.update);
route.delete('/api/courses/:id', controller.delete);

module.exports = route;