const express = require('express');
const morgan = require('morgan');
const bodyparser = require('body-parser'); 
const dotenv = require('dotenv');
const { graphqlHTTP }= require('express-graphql');
const { graphQLschema } = require('./graphql-schema')
const Order = require('./server/models/orderModel');
const Client = require('./server/models/clientModel');
const Product = require('./server/models/productModel');
const connectDB = require('./server/database/connections');
const app = express();
 
dotenv.config({path : 'config.env'});
const PORT = process.env.PORT || 8080;

app.use(morgan('tiny'));
   
connectDB(); 

app.use(bodyparser.urlencoded({extended : true}));
  
  /*Agregar cliente*
   * 
   * @param {*} req 
   */
  const addClient = (req) => {
    const client = new Client.model();
    client.name = req.name;
    client.lastName = req.lastName;
    client.email = req.email;
  
    client.save();
    return client;
  }
  
  /*Agregar producto*
   * 
   * @param {*} req 
   */
  const addProduct = (req) => {
    const product = new Product.model();
    product.name = req.name;
  
    product.save();
    return product;
  }
  
  /*Agregar Orden*
   * 
   * @param {*} req 
   */
  const addOrder = async (req) => {
    const client = await Client.model.findById({ _id: req.clientId }).exec();
    const product = await Product.model.findById({ _id: req.productId }).exec();
  
    const order = new Order()
  
    order.client = client;
    order.products.push(product)
    order.save()
  
    return order;
  }
  
    /*Obtner ordenes*
   * 
   */
    const getOrders = () => {
        return Order.find(function (err, orders) {
          if (err) {
            return "There was an error"
          }
          return orders;
        })
      };
      
  // expose in the root element the different entry points of the
  // graphQL service
  const root = {
    addClient: (req) => addClient(req),
    addProduct: (req) => addProduct(req),
    addOrder: (req) => addOrder(req),
    orders: (req, res) => getOrders(req)
  };
  
  // check for cors
  const cors = require("cors");
  app.use(cors({
    domains: '*',
    methods: "*"
  }));
  
  //one single endpoint different than REST
app.use('/graphql', graphqlHTTP({
    schema: graphQLschema,
    rootValue: root,
    graphiql: true,
}));


app.listen(PORT, ()=>{
    console.log(`Servidor corriendo en localhost:${PORT}`);
});