const express = require('express');
const bodyparser = require('body-parser');
const cors = require("cors");
const fetch = require('node-fetch');

const app = express();

const PORT = 3000 || 8080;
 
//parsea el body
app.use(bodyparser.urlencoded({extended : true}));

app.use(cors({
  domains: '*',
  methods: "*"
}));

app.get('/brewe', async (req, res) => {
  try {

    const response = await fetch('https://api.openbrewerydb.org/breweries?per_page=10');
    const json = await response.json();
    
    res.status(200);
    res.json(json);

  } catch (error) {
    console.log(error.response.body);
  }
});

app.listen(PORT, ()=>{
    console.log(`Servidor corriendo en localhost:${PORT}`);
});