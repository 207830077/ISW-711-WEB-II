const express = require('express');
const morgan = require('morgan');
const bodyparser = require('body-parser'); 
const dotenv = require('dotenv');
const connectDB = require('./server/database/connection');

const app = express();

dotenv.config({path : 'config.env'});
const PORT = process.env.PORT || 8080;

//registra solicitudes HTTP y actualiza el estado y el tiempo de respuesta
app.use(morgan('tiny'));

//conexion mongoDB
connectDB(); 

//parsea el body
app.use(bodyparser.urlencoded({extended : true}));

//carga las rutas
app.use('/', require('./server/routes/router'));

//pueto que escucha
app.listen(PORT, ()=>{
    console.log(`Servidor corriendo en localhost:${PORT}`);
});