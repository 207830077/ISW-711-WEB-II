const mongoose =  require('mongoose');

var task = new mongoose.Schema({
    title: { type: String },
    description: { type: String }
});

const TaskDB = mongoose.model('task', task);
module.exports = TaskDB;