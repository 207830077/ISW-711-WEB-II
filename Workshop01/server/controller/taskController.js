var taskDB = require('../model/taskModel');

//crear y guardar la tarea
exports.create = async (req, res)=>{
    //validar el request
    if(!req.body){
        res.status(400);
        res.json({error : 'Contenido vacio'});
    }
    
    //Tarea
    const task = new taskDB({
        title : req.body.title,
        description : req.body.description
    })

    //guardar en la base de datos
    const saveTask = await task.save();
    if(saveTask){
        res.status(200);
        res.json(saveTask);
    } else{
        res.status(500);
        res.json({error: 'Error al crear la tarea'});
    }
}

//obtener las tareas
exports.find = async (req, res)=>{
    if(req.query.id){
        const id = req.query.id;
        const taskById = await taskDB.findById(id);
        if(taskById){
            res.status(200);
            res.json(taskById);
        }else{
            res.status(500);
            res.json({error: 'Error al obtener la tarea'});
        }
    }else{
        const task = await taskDB.find();
        if(task){
            res.status(200);
            res.json(task);
        }else{
            res.status(500);
            res.json({error: 'Error al obtener las tareas'});
        }
    }
};

//actualizar tarea
exports.update = async (req, res)=>{
    if(!req.body){
        res.status(400);
        res.json({error : 'Contenido vacio para actualizar'});
    }
    const id = req.params.id;
    const taskUpdate = await taskDB.findByIdAndUpdate(id, req.body, {useFindAndModify: false}); 
    if(taskUpdate){
        res.status(200);
        res.json(taskUpdate);
    }else{
        res.status(500);
        res.json({error: 'Error al actualizar la tarea'});
    }
}

//eliminar tareas
exports.delete = async (req, res)=>{
    const id = req.params.id;
    const taskDelete = await taskDB.findByIdAndDelete(id); 
    if(taskDelete){
        res.status(200);
        res.json(taskUpdate);
    }else{
        res.status(500);
        res.json({error: 'Error al eliminar la tarea'});
    }
};