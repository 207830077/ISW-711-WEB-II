const express = require('express');
const route = express.Router();

const taskController = require('../controller/taskController')

route.get('/', (req, res) =>{
    res.send('Workshop 01');
});

//API
route.post('/api/tasks', taskController.create);
route.get('/api/tasks', taskController.find);
route.put('/api/tasks/:id', taskController.update);
route.delete('/api/tasks/:id', taskController.delete);

module.exports= route;